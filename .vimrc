" enable syntax processing
" - what exactly does it do?
syntax enable

" So tabs don't go ALL THE WAY TO THE OTHER SIDE of the screen.
set tabstop=4

" This is the same as above, but for when editing (and not just viewing)
set softtabstop=4

" This determines how far >>, <<, and == will move the line indentation
" (also fixes 'filetype plugin' autoindent feat. to indent newlines nicely)
set shiftwidth=4

" TABS TO SPACES
set expandtab

" Automatcally indent newline based on filetype
filetype plugin indent on

" Show Line #s
set number

" Highlight the current line
set cursorline

" Speeds up stuff like macros; don't redraw screen for every little thing.
set lazyredraw

" Matches parentheses & what-not.
set showmatch


" --------------------------------------
" SEARCHES -----------------------------
" --------------------------------------

" Incremental search as you type; like Google search, in a way
set incsearch

" Highlight search results
set hlsearch

" Folding -- add this later, looks useful.
" set foldenable
" set foldstartlevel=10 "opens all folds 10 lvls deep on loading a file w/ folds
" set foldnestmax=10 " max lvls of folds you're allowed to set
" set foldmethod=indent " folds based on indent. Other ways --> :help foldmethod


" SEE https://dougblack.io/words/a-good-vimrc.html for more tips & cool things.
" - [plugin] The Silver Searcher for fast searching in projects
" - mksession to save all files
" - Fuzzy searching --> CtrlP [plugin] + Silver Searcher (see above)
" - Tmux & how to get the vertical cursor on edit (instead of always block cursor)
" - How to get Vim to automatically trim extraneous whitespace
" - How to set up a tmp/Backups folder so that Vim doesn't leave a ton of recoveryfiles all over
